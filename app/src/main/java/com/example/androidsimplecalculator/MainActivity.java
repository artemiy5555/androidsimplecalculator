package com.example.androidsimplecalculator;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button button = (Button) findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Spinner spinner = findViewById(R.id.tv_spinnerOperator);
                String operator = spinner.getSelectedItem().toString();
                int oneNumber = 0;
                int twoNumber = 0;
                EditText editTextOne = findViewById(R.id.tv_OneTestField);
                EditText editTextTwo = findViewById(R.id.tv_TwoTestField);
                try {
                    oneNumber = Integer.parseInt(editTextOne.getText().toString());
                    twoNumber = Integer.parseInt(editTextTwo.getText().toString());
                }catch (Exception e){
                    Toast.makeText(getApplicationContext(), "Введите числа!", Toast.LENGTH_SHORT).show();
                    return;
                }
                System.out.println(oneNumber);
                System.out.println(twoNumber);

                double result;
                switch (operator) {
                    case "+":  result = oneNumber+twoNumber;
                        break;
                    case "-":  result = oneNumber-twoNumber;
                        break;
                    case "*":  result = oneNumber*twoNumber;
                        break;
                    case "/":
                        if(twoNumber==0) {
                            Toast.makeText(getApplicationContext(), "На ноль не делим!", Toast.LENGTH_SHORT).show();
                        return;
                    }else
                        result = oneNumber/twoNumber;
                        break;
                    default: result = 0;
                        break;
                }

                Toast.makeText(getApplicationContext(), String.valueOf(result), Toast.LENGTH_SHORT).show();
            }
        });


    }
}